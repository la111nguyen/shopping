package com.ecommerce.users.usersmicroservice.service;

import com.ecommerce.users.usersmicroservice.model.User;
import com.ecommerce.users.usersmicroservice.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserServices.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class UserServiceTest {

    @Autowired
    private UserServices userServices;
    @MockBean
    private UserRepository userRepository;

    @Test
    public void getAllUser() {
        // prepare data and mock's behaviour
        List<User> userList = buildUser();
        when(userServices.getAllUser()).thenReturn(userList);
        // execute
        when(userRepository.findAll()).thenReturn(userList);
        List<User> listUserResponse = userServices.getAllUser();
        assertThat(listUserResponse, hasSize(2));
        assertThat(listUserResponse.get(0).getId(), equalTo(1L));
        assertThat(listUserResponse.get(1).getId(), equalTo(2L));
    }

    private List<User> buildUser() {
        User u1 = new User(1L, "Lam", "Nguyen", new Date());
        User u2 = new User(2L, "Hoang", "Pham", new Date());
        List<User> userList = Arrays.asList(u1, u2);
        return userList;
    }
}