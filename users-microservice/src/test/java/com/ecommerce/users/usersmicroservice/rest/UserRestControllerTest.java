package com.ecommerce.users.usersmicroservice.rest;

import com.ecommerce.users.usersmicroservice.TestUtils;
import com.ecommerce.users.usersmicroservice.model.User;
import com.ecommerce.users.usersmicroservice.service.UserServices;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(UserRestController.class)
@ImportAutoConfiguration({RibbonAutoConfiguration.class, FeignRibbonClientAutoConfiguration.class, FeignAutoConfiguration.class})
public class UserRestControllerTest {

    private final String URL = "/";
    @MockBean
    UserServices userService;

    @MockBean
    OrderClient orderClient;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetUser() throws Exception {

        // prepare data and mock's behaviour
        User userStub = new User(1L, "Lam", "Nguyen", new Date());
        when(userService.getUser(any(Long.class))).thenReturn(userStub);
        // execute
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get(URL + "{id}", 1L).accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        // verify
        int status = result.getResponse().getStatus();
        assertEquals("OK Response Status", HttpStatus.OK.value(), status);
        // verify that service method was called once
        verify(userService).getUser(any(Long.class));
        User resultUser = TestUtils.jsonToObject(result.getResponse().getContentAsString(), User.class);
        assertNotNull(resultUser);
        assertEquals(1l, resultUser.getId().longValue());
    }


    @Test
    public void testGetAllUser() throws Exception {

        // prepare data and mock's behaviour
        List<User> userList = buildUser();
        when(userService.getAllUser()).thenReturn(userList);

        // execute
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(URL).accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        // verify
        int status = result.getResponse().getStatus();
        assertEquals("OK Response Status", HttpStatus.OK.value(), status);

        // verify that service method was called once
        verify(userService).getAllUser();

        // get the List<User> from the Json response
        TypeToken<List<User>> token = new TypeToken<List<User>>() {
        };
        @SuppressWarnings("unchecked")
        List<User> userListResult = TestUtils.jsonToList(result.getResponse().getContentAsString(), token);

        assertNotNull("User list found", userListResult);
        assertEquals("OK User List", userList.size(), userListResult.size());

    }

    private List<User> buildUser() {
        User u1 = new User(1L, "Lam", "Nguyen", new Date());
        User u2 = new User(2L, "Hoang", "Pham", new Date());
        List<User> userList = Arrays.asList(u1, u2);
        return userList;
    }
}
