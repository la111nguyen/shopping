package com.ecommerce.users.usersmicroservice.rest;

import org.springframework.stereotype.Component;
import org.springframework.http.*;
import java.util.Collections;
import java.util.List;
import com.ecommerce.users.usersmicroservice.model.Orders;

@Component
public class OrderClientFallback implements OrderClient {
    @Override
    public  ResponseEntity<List<Orders>>  getOrdersForUser(Long userId) {
        return ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList());
    }
}
