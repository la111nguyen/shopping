package com.ecommerce.users.usersmicroservice.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.ecommerce.users.usersmicroservice.model.User;
import com.ecommerce.users.usersmicroservice.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class UserServices {
	private static final Logger logger = LoggerFactory.getLogger(UserServices.class);

	@Autowired
	UserRepository userRepository;

	public List<User> getAllUser() {
		try {
			return userRepository.findAll();
		} catch (Exception e) {
			logger.error("UserServices.getAllUser error. Message - {}", e.getMessage());
			return null;
		}
	}

	public User getUser(Long id) {
		try {
			return userRepository.findById(id).get();
		} catch (NoSuchElementException e) {
			logger.error("UserServices.getUser error. Message - {}", e.getMessage());
			return null;
		}
	}
}
