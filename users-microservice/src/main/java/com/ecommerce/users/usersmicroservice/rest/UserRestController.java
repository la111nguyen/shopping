package com.ecommerce.users.usersmicroservice.rest;

import com.ecommerce.users.usersmicroservice.model.Orders;
import com.ecommerce.users.usersmicroservice.model.User;
import com.ecommerce.users.usersmicroservice.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/api")
public class UserRestController {

	@Autowired
	UserServices userService;

	private OrderClient orderClient;

	public UserRestController(OrderClient orderClient) {
		this.orderClient = orderClient;
	}

	@GetMapping("/{userId}/orders")
	public ResponseEntity<List<Orders>> getOrdersForUser(@PathVariable Long userId) {
		return orderClient.getOrdersForUser(userId);
	}

	@GetMapping
	public ResponseEntity<List<User>> getAllUser() {
		try {
			List<User> users=userService.getAllUser();
			if (users != null) {
	            return ResponseEntity.status(HttpStatus.OK).body(users);
	        } else {
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	        }
			}catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/{id}")
		public ResponseEntity<User> getUser(@PathVariable Long id) {
		try {
		User user=userService.getUser(id);
		if (user != null) {
            return ResponseEntity.status(HttpStatus.OK).body(user);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
		}catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
