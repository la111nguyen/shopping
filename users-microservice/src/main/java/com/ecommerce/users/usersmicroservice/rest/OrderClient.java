package com.ecommerce.users.usersmicroservice.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;
import java.util.List;
import com.ecommerce.users.usersmicroservice.model.Orders;

@FeignClient(name = "orders-microservice", fallback = OrderClientFallback.class)
public interface OrderClient {

    @RequestMapping(method = RequestMethod.GET,value="/user/{userId}")
    ResponseEntity<List<Orders>>  getOrdersForUser(@PathVariable(name = "userId")Long userId);
}
