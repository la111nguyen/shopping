package com.ecommerce.products.productsmicroservice.service;

import com.ecommerce.products.productsmicroservice.model.Products;
import com.ecommerce.products.productsmicroservice.repository.ProductRepository;
import com.ecommerce.products.productsmicroservice.services.ProductServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductServices.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ProductServiceTest {

    @Autowired
    private ProductServices productServices;
    @MockBean
    private ProductRepository productRepository;

    @Test
    public void getAllProduct() {
        // prepare data and mock's behaviour
        List<Products> productList = buildProducts();
        when(productServices.getAllProdcuts()).thenReturn(productList);
        // execute
        when(productRepository.findAll()).thenReturn(productList);
        List<Products> listProductResponse = productServices.getAllProdcuts();
        assertThat(listProductResponse, hasSize(2));
        assertThat(listProductResponse.get(0).getId(), equalTo(1L));
        assertThat(listProductResponse.get(1).getId(), equalTo(2L));
    }

    private List<Products> buildProducts() {
        Products p1 = new Products(1L, "T-shirt", "Blue", new BigDecimal(10), new BigDecimal(10));
        Products p2 = new Products(2L, "Jacket", "Black", new BigDecimal(10), new BigDecimal(10));
        List<Products> productList = Arrays.asList(p1, p2);
        return productList;
    }
}