package com.ecommerce.products.productsmicroservice.rest;

import com.ecommerce.products.productsmicroservice.TestUtils;
import com.ecommerce.products.productsmicroservice.model.Products;
import com.ecommerce.products.productsmicroservice.services.ProductServices;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductsRestController.class)

public class ProductsRestControllerTest {

    private final String URL = "/";
    @MockBean
    ProductServices productServices;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetProduct() throws Exception {
        // prepare data and mock's behaviour
        Products p1 = new Products(1L, "T-shirt", "Blue", new BigDecimal(10), new BigDecimal(10));
        when(productServices.getProduct(any(Long.class))).thenReturn(p1);
        // execute
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get(URL + "{id}", 1L).accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        // verify
        int status = result.getResponse().getStatus();
        assertEquals("OK Response Status", HttpStatus.OK.value(), status);
        // verify that service method was called once
        verify(productServices).getProduct(any(Long.class));
        Products resultProduct = TestUtils.jsonToObject(result.getResponse().getContentAsString(), Products.class);
        assertNotNull(resultProduct);
        assertEquals(1l, resultProduct.getId());
    }


    @Test
    public void testGetAllUser() throws Exception {

        // prepare data and mock's behaviour
        List<Products> productList = buildProducts();
        when(productServices.getAllProdcuts()).thenReturn(productList);

        // execute
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(URL).accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        // verify
        int status = result.getResponse().getStatus();
        assertEquals("OK Response Status", HttpStatus.OK.value(), status);

        // verify that service method was called once
        verify(productServices).getAllProdcuts();

        // get the List<Products> from the Json response
        TypeToken<List<Products>> token = new TypeToken<List<Products>>() {
        };
        @SuppressWarnings("unchecked")
        List<Products> productListResult = TestUtils.jsonToList(result.getResponse().getContentAsString(), token);

        assertNotNull("Product list found", productListResult);
        assertEquals("OK Product List", productList.size(), productListResult.size());

    }

    private List<Products> buildProducts() {
        Products p1 = new Products(1L, "T-shirt", "Blue", new BigDecimal(10), new BigDecimal(10));
        Products p2 = new Products(2L, "Jacket", "Black", new BigDecimal(10), new BigDecimal(10));
        List<Products> productList = Arrays.asList(p1, p2);
        return productList;
    }
}
