package com.ecommerce.products.productsmicroservice.rest;

import com.ecommerce.products.productsmicroservice.model.ProductDetails;
import com.ecommerce.products.productsmicroservice.model.Products;
import com.ecommerce.products.productsmicroservice.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class ProductsRestController {

    @Autowired
    private ProductServices productsService;


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Products>> getAllProducts() {
        try {
            List<Products> products = productsService.getAllProdcuts();
            if (products != null) {
                return ResponseEntity.status(HttpStatus.OK).body(products);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Products> getProducts(@PathVariable Long id) {
        try {
            Products product = productsService.getProduct(id);
            if (product != null) {
                return ResponseEntity.status(HttpStatus.OK).body(product);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Products> addProducts(@RequestBody ProductDetails productdetails) {
        try {

            Products product = new Products(productdetails.getName(), productdetails.getDescription(),
                    productdetails.getQuantity(), productdetails.getPrice());
            product = productsService.addProduct(product);

            if (product != null) {
                return ResponseEntity.status(HttpStatus.OK).body(product);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity deleteProducts(@PathVariable Long id) {
        try {
            productsService.deleteProduct(id);
            return ResponseEntity.status(HttpStatus.OK).body("Deleted");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
