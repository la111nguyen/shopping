package com.ecommerce.products.productsmicroservice.services;

import com.ecommerce.products.productsmicroservice.model.Products;
import com.ecommerce.products.productsmicroservice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class ProductServices {

    @Autowired
    private ProductRepository productRepository;


    public List<Products> getAllProdcuts() {
        return productRepository.findAll();
    }

    public Products getProduct(@PathVariable Long id) {
        return productRepository.findById(id).get();
    }

    public Products addProduct(Products products) {
        return productRepository.save(products);
    }

    public void updateProduct(Products products) {
        productRepository.save(products);
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
