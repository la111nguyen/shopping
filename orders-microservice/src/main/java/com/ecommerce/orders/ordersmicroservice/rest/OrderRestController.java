package com.ecommerce.orders.ordersmicroservice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.orders.ordersmicroservice.model.Orders;
import com.ecommerce.orders.ordersmicroservice.services.OrdersService;

@RestController

public class OrderRestController {

	@Autowired
	private OrdersService ordersService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Orders>> getAllOrder() {
		try {
			List<Orders> orders=ordersService.getAllOrders();
			if (orders != null) {
	            return ResponseEntity.status(HttpStatus.OK).body(orders);
	        } else {
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	        }
			}catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/user/{userId}")
	public ResponseEntity<List<Orders>> getAllOrderByUserId(@PathVariable(name = "userId")Long userId) {
		try {
			List<Orders> orders=ordersService.getAllOrdersByUser(userId);
			if (orders != null) {
	            return ResponseEntity.status(HttpStatus.OK).body(orders);
	        } else {
	            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	        }
			}catch (Exception e) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
	}

}
