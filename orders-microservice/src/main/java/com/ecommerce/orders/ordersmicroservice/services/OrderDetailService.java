package com.ecommerce.orders.ordersmicroservice.services;

import com.ecommerce.orders.ordersmicroservice.model.OrderDetail;
import com.ecommerce.orders.ordersmicroservice.repository.OrderDetailRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailService {
    private static final Logger logger = LoggerFactory.getLogger(OrderDetailService.class);

    @Autowired
    OrderDetailRepository orderDetailRepository;

    public List<OrderDetail> getAllOrderDetail(Long orderId) {
        try {
            List<OrderDetail> listOrderDetail = orderDetailRepository.findByOrderId(orderId);
            System.out.println(listOrderDetail.size());
            return listOrderDetail;
        } catch (Exception e) {
            logger.error("OrderDetailService.getAllOrderDetail error. Message - {}", e.getMessage());
            return null;
        }
    }

}
