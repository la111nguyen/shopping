package com.ecommerce.orders.ordersmicroservice.services;

import com.ecommerce.orders.ordersmicroservice.model.Orders;
import com.ecommerce.orders.ordersmicroservice.repository.OrdersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OrdersService {
    private static final Logger logger = LoggerFactory.getLogger(OrdersService.class);

    @Autowired
    private OrdersRepository ordersRepository;

    public List<Orders> getAllOrders() {
        try {
            return ordersRepository.findAll();
        } catch (Exception e) {
            logger.error("OrdersService.getAllOrders error. Message - {}", e.getMessage());
            return null;
        }
    }

    public List<Orders> getAllOrdersByUser(Long userId) {
        try {
            return ordersRepository.findByUserId(userId);
        } catch (Exception e) {
            logger.error("OrdersService.getAllOrdersByUser error. Message - {}", e.getMessage());
            return null;
        }
    }

    public Orders getOrder(Long id) {
        try {
            return ordersRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            logger.error("OrdersService.getOrder error. Message - {}", e.getMessage());
            return null;
        }
    }

    public void addOrder(Orders order) {
        ordersRepository.save(order);
    }

    public void orderOrder(Orders order) {
        ordersRepository.save(order);
    }

    public void deleteOrder(Long id) {
        ordersRepository.deleteById(id);
    }
}
