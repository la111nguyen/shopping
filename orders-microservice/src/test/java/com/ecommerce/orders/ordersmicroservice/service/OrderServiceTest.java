package com.ecommerce.orders.ordersmicroservice.service;

import com.ecommerce.orders.ordersmicroservice.model.Orders;
import com.ecommerce.orders.ordersmicroservice.model.User;
import com.ecommerce.orders.ordersmicroservice.repository.OrdersRepository;
import com.ecommerce.orders.ordersmicroservice.services.OrdersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrdersService.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class OrderServiceTest {

    @Autowired
    private OrdersService orderServices;
    @MockBean
    private OrdersRepository orderRepository;

    @Test
    public void getAllUser() {
        // prepare data and mock's behaviour
        List<Orders> orderList = buildOrders();
        when(orderServices.getAllOrders()).thenReturn(orderList);
        // execute
        when(orderRepository.findAll()).thenReturn(orderList);
        List<Orders> listOrderResponse = orderServices.getAllOrders();
        assertThat(listOrderResponse, hasSize(2));
    }

    private List<Orders> buildOrders() {
        User u1 = new User("Hoang", "Pham", new Date());
        User u2= new User("Phong", "Tran", new Date());
        //Orders(java.sql.Date orderDate, Double total, Double discount, User user)
        Orders o1 = new Orders(new Date(),10.0, 10.0, u1);
        Orders o2 = new Orders(new Date(),10.0, 10.0, u2);

        List<Orders> orderList = Arrays.asList(o1, o2);
        return orderList;
    }
}