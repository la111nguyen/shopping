package com.ecommerce.orders.ordersmicroservice.rest;

import com.ecommerce.orders.ordersmicroservice.TestUtils;
import com.ecommerce.orders.ordersmicroservice.model.Orders;
import com.ecommerce.orders.ordersmicroservice.model.User;
import com.ecommerce.orders.ordersmicroservice.services.OrdersService;
import com.google.gson.reflect.TypeToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderRestController.class)

public class OrderRestControllerTest {

    private final String URL = "/";
    @MockBean
    OrdersService ordersService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllOrders() throws Exception {

        // prepare data and mock's behaviour
        List<Orders> orderList = buildOrders();
        when(ordersService.getAllOrders()).thenReturn(orderList);

        // execute
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(URL).accept(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();

        // verify
        int status = result.getResponse().getStatus();
        assertEquals("OK Response Status", HttpStatus.OK.value(), status);

        // verify that service method was called once
        verify(ordersService).getAllOrders();

        // get the List<Orders> from the Json response
        TypeToken<List<Orders>> token = new TypeToken<List<Orders>>() {
        };
        @SuppressWarnings("unchecked")
        List<Orders> orderListResult = TestUtils.jsonToList(result.getResponse().getContentAsString(), token);

        assertNotNull("Order list found", orderListResult);
        assertEquals("OK Order List", orderList.size(), orderListResult.size());
    }

    private List<Orders> buildOrders() {
        User u1 = new User("Hoang", "Pham", new Date());
        User u2 = new User("Phong", "Tran", new Date());
        //Orders(java.sql.Date orderDate, Double total, Double discount, User user)
        Orders o1 = new Orders(new Date(), 10.0, 10.0, u1);
        Orders o2 = new Orders(new Date(), 10.0, 10.0, u2);

        List<Orders> orderList = Arrays.asList(o1, o2);
        return orderList;
    }
}
