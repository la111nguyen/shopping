# Ecommerce-Microservices

-------------
Overview |
-------------
In this project I'm demonstrating you the most interesting features of Spring Cloud Project for building microservice-based architecture.
This project is for understanding how spring boot microservices can be created and how can they communicate with each other. 

---------
Technology Stack |
---------
* Spring Boot Projects as Microservices
* Spring Cloud microservices 
* Eureka & Server Client as Discovery Server
* Zuul Proxy 
* Hystrix Circuit Breaker for Fault tolerance & Resilience
* Spring Cloud OpenFeign, a declarative web service client for communication with each service 
* Rest API
* JPA
* hibernate
* MYSQL
* Spring Boot Log: Slf4J delagate
* JUnit and Mockito

## Architecture
![eureka server](architecture.gif)

Our sample microservices-based system consists of the following modules:
- **gateway-service** - a module that Spring Cloud Netflix Zuul for running Spring Boot application that acts as a proxy/gateway in our architecture.
- **discovery-service** - a module that depending on the example it uses Spring Cloud Netflix Eureka as an embedded discovery server.
- **orders-microservice** - a module to handle orders
- **products-microservice** - a module to handle products.
- **user-microservice** - a module to manage user. It communicates with orders-microservice to get all orders of user.
- **Service’s database** - private-tables-per-service – each service owns a set of tables that must only be accessed by that service.

## Reference Documentation and Source code
    -https://blog.scottlogic.com/2019/10/31/building-microservices-with-spring-boot.html
    -https://github.com/bhagyesh18/Ecommerce-Microservices

## Building and running this application

To build and run this application, follow these steps:

1. Open a command line window or terminal.
2. Navigate to the root directory of each project, where the `pom.xml` resides.
3. Compile the project: `mvn clean install`.

### Running the service discovery application

1. Open a command line window or terminal and navigate to the `discovery-service` folder.
2. Start the `discovery-service` application: `java -jar target\discovery-service-0.0.1-SNAPSHOT.jar`.
3. A Netflix Eureka console will be available at `http://localhost:3000`.

### Running the product service application

1. Open a command line window or terminal and navigate to the `products-microservice` folder.
2. Start the `products-microservice` application: `java -jar target\products-microservice-0.0.1-SNAPSHOT.jar`
3. This service will start on the port `3001` and it will automatically register itself in the service discovery. Check the Eureka console.

### Running the order service application

1. Open a command line window or terminal and navigate to the `orders-microservice` folder.
2. Start the `orders-microservice` application: `java -jar target\orders-microservice-0.0.1-SNAPSHOT.jar`
3. This service will start on the port `3002` and it will automatically register itself in the service discovery. Check the Eureka console.

### Running the user service application

1. Open a command line window or terminal and navigate to the `users-microservice` folder.
2. Start the `users-microservice` application: `java -jar target\users-microservice-0.0.1-SNAPSHOT.jar`
3. This service will start on the port `3003` and it will automatically register itself in the service discovery. Check the Eureka console.

### Running the API gateway application

1. Open a command line window or terminal and navigate to the `gateway-service` folder.
2. Start the `gateway-service` application: `java -jar target\gateway-service-0.0.1-SNAPSHOT.jar`.
3. This service will start on the port `8888` and it will automatically register itself in the service discovery. Check the Eureka console.

### Install and configure database
1. Install Mysql (using with MySql Workbench verion 8.0).
2. Create `ecommerce` schema
3. Update database information in each service including orders, products and users. For example:
    - **spring.datasource.url=jdbc:mysql://localhost:3306/ecommerce**
    - **spring.datasource.username=root**
    - **spring.datasource.password=root**
4. If you need data to verify, you can import the dump file in `database/ecommerce20200327.sql`

## REST API overview (Window 10)

The application provides a REST API for managing tasks. See the [curl][] scripts below with the supported operations:

##### Get all products
```
curl -X GET "http://localhost:8888/products" -H "Accept: application/json"
```
##### Get product by id
```
curl -X GET "http://localhost:8888/products/{product-id}" -H "Accept: application/json"
```
##### Add product
```
curl -i -X POST -H "Content-Type:application/json" -d "{\"name\": \"Jean\", \"description\": \"Men Blue Jean\", \"quantity\": 10, \"price\": 10}" http://localhost:8888/products
```
##### Delete product by id
```
curl -X DELETE  "http://localhost:8888/products/{product-id}"
```

##### Get all orders
```
curl -X GET "http://localhost:8888/orders" -H "Accept: application/json"
```

##### Get all orders by user
```
curl -X GET "http://localhost:8888/orders/user/{userid}" -H "Accept: application/json"
```
##### Get all orders detail
```
curl -X GET "http://localhost:8888/orders/{ordersid}/products" -H "Accept: application/json"
```

##### Get all users
```
curl -X GET "http://localhost:8888/users" -H "Accept: application/json"
```

##### Get user by id
```
curl -X GET "http://localhost:8888/users/{userid}" -H "Accept: application/json"
```
##### Get user's orders (user service call to order service to get user's order)
```
curl -X GET "http://localhost:8888/users/{userId}/orders" -H "Accept: application/json"
```

